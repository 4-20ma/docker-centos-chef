# encoding: utf-8
#
# Dockerfile:: docker-centos-chef
#
# Author:: Doc Walker (<4-20ma@wvfans.net>)
#
# Copyright 2016 Doc Walker
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#----------------------------------------------------------------------- usage
# run lint checks on Dockerfile
# $ docker run -it --rm --privileged -v $(pwd):/root/ projectatomic/dockerfile-lint dockerfile_lint --rulefile lint/default_rules.yaml
#
# update to latest upstream container
# $ docker pull centos:centos6
#
# create new image using centos:6 as base + latest version of chef
# $ docker build --no-cache -t 420ma/centos-chef:centos6 .
# $ docker build -t 420ma/centos-chef:centos6 .
#
# access container shell
# $ docker run -it 420ma/centos-chef:centos6
#
# push to hub.docker.com
# $ docker push 420ma/centos-chef:centos6
#

#----------------------------------------------------------------------- image
FROM centos:centos6
MAINTAINER Doc Walker <4-20ma@wvfans.net>
LABEL name="CentOS Base Image with Chef 12" \
    description="Image used to run integration tests on CentOS 6-based Chef cookbooks." \
    vendor="4-20ma" \
    license="apache-2.0" \
    build-date="2017-02-21"

# invalidate container cache on changed Dockerfile
COPY Dockerfile /etc/

# update existing packages, install new packages, clean up
# WORKAROUND: Docker issue #783 'Rpmdb checksum is invalid'
RUN yum update -y --noplugins && yum install -y --noplugins \
  automake \
  gcc \
  git \
  sudo \
  ; yum clean all --noplugins; touch /var/lib/rpm/*

# install Chef server
RUN curl -L https://omnitruck.chef.io/install.sh | bash -s -- -v 12.18.31

# resolve berkshelf 'Encoding::InvalidByteSequenceError: "\xE2" on US-ASCII'
ENV LANG en_US.UTF-8

# set path so embeded ruby is visible by #!/usr/bin/env ruby
ENV PATH $PATH:/opt/chef/embedded/bin

# WORKAROUND: 'can't find executable bundle (Gem::Exception)'
# update file /opt/chef/embedded/bin/bundle
# replace 'activate_bin_path' with 'bin_path' in file
RUN bundle_path=$(which bundle) && \
  sed -i -e "s/activate_bin_path/bin_path/g" $bundle_path

CMD ["/bin/bash"]
