# CHANGELOG

## [v1.1.0](https://gitlab.com/4-20ma/docker-centos-chef/tree/v1.1.0) (2017-02-21)
[Full Changelog](https://gitlab.com/4-20ma/docker-centos-chef/compare/v1.0.0...v1.1.0)

**Implemented enhancements:**

- Update/pin chef to v12.18.31 (embedded ruby-2.3.1p112) [!4](https://gitlab.com/4-20ma/docker-centos-chef/merge_requests/4) ([4-20ma](https://gitlab.com/4-20ma))

## [v1.0.0](https://gitlab.com/4-20ma/docker-centos-chef/tree/v1.0.0) (2016-08-26)

- `NEW` - initial release


## Legend

- `BREAK`   - breaking changes
- `FIX`     - bug fix
- `IMPROVE` - improvement of existing feature
- `NEW`     - new feature
