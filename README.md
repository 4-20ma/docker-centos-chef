# docker-centos-chef

[![release](https://img.shields.io/badge/release-1.1.0-blue.svg?maxAge=3600)][release]
[![Chef server](https://img.shields.io/badge/chef--server-12.18.31-blue.svg?maxAge=3600)][Chef server]

[release]:          https://gitlab.com/4-20ma/docker-centos-chef
[Chef server]:      https://downloads.chef.io/chef-server/

# Supported tags and respective `Dockerfile` links

- [`centos6` (_Dockerfile_)](https://gitlab.com/4-20ma/docker-centos-chef/blob/master/Dockerfile)

For more information about this image and its history, please see [the relevant CHANGELOG file (`CHANGELOG.md`)](https://gitlab.com/4-20ma/docker-centos-chef/blob/master/CHANGELOG.md). This image is updated via [merge requests to the `4-20ma/docker-centos-chef` GitLab repo](https://gitlab.com/4-20ma/docker-centos-chef/merge_requests).

## Overview

This image is used to run integration tests on CentOS 6-based Chef cookbooks. It's based on `centos:centos6` with minimal additional software installed to facilitate automated continuous integration builds via `gitlab-ci.com`.


## Usage

File `.gitlab-ci.yml`

    image: 420ma/centos-chef:centos6

    before_script:
      - git submodule update --init --recursive
      - ruby -v
      - which ruby
      - bundle install --deployment --without development --retry 3 --jobs $(nproc)

    cache:
      paths:
        - vendor/bundle/

    style:
      script:
        - bundle exec rake style

    spec:
      script:
        - bundle exec rake spec

    integration:
      variables:
        KITCHEN_YAML: .kitchen.gitlab-ci.yml
        KITCHEN_PLATFORM: centos-6
      script:
        - bundle exec rake kitchen
    ...


## Development

Run lint checks on `Dockerfile`

    $ docker run -it --rm --privileged -v $(pwd):/root/ projectatomic/dockerfile-lint dockerfile_lint --rulefile lint/default_rules.yaml

Build image (container cache enabled; modify `Dockerfile` to invalidate cache)

    $ docker build -t 420ma/centos-chef:centos6 .

Build image, explicitly disabling container cache

    $ docker build --no-cache -t 420ma/centos-chef:centos6 .

Run container (verification/troubleshooting)

    $ docker run -it 420ma/centos-chef:centos6

Run integration tests

    $ rspec

Push to hub.docker.io

    $ docker push 420ma/centos-chef:centos6


## Updates/Packages

- yum updates
- `automake`
- `gcc`
- `git`
- `sudo`
- Chef Server via Omnitruck package


## Fixes/Workarounds

- WORKAROUND `touch /var/lib/rpm/*` to work around 'Rpmdb checksum is invalid' ([Docker issue #783](https://github.com/docker/docker/issues/783))
- FIX set `LANG` to resolve berkshelf `Encoding::InvalidByteSequenceError`
- FIX set `PATH` to allow embedded chef ruby to work in continuous integration environment
- FIX modify `bundle` method to allow it to work with rubygems in continuous integration environment


## License

```
Copyright 2016 Doc Walker

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
