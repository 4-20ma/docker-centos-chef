# encoding: utf-8
require 'docker'
require 'json'
require 'serverspec'

describe 'Dockerfile' do
  #-------------------------------------------------------------------- before
  before(:all) do
    Docker.options[:read_timeout] = 300
    @image = Docker::Image.build_from_dir('.') do |v|
      begin
        log = JSON.parse(v)
      rescue JSON::ParserError
        log = { 'stream' => v }
      end
      $stdout.puts log['stream'] if log.respond_to?(:[])
    end

    set :os, family: :redhat
    set :backend, :docker
    set :docker_image, @image.id
  end # before

  #--------------------------------------------------------------------- after
  after(:all) do
    @image.remove(:force => true)
  end # after

  #------------------------------------------------------------------- package
  it 'installs expected CentOS release' do
    expect(os_release).to include('centos-release-6')
  end # it

  describe package('automake') do
    it 'is installed' do
      expect(subject).to be_installed
    end
  end # describe

  describe package('gcc') do
    it 'is installed' do
      expect(subject).to be_installed
    end
  end # describe

  describe package('git') do
    it 'is installed' do
      expect(subject).to be_installed
    end
  end # describe

  describe package('sudo') do
    it 'is installed' do
      expect(subject).to be_installed
    end
  end # describe

  #--------------------------------------------------------------- environment
  describe 'LANG' do
    it 'sets environment variable to expected valur' do
      expect(docker_env(subject)).to include('en_US.UTF-8')
    end # it
  end # describe

  describe 'PATH' do
    it 'sets environment variable to expected valur' do
      expect(docker_env(subject)).to include('/opt/chef/embedded/bin')
    end # it
  end # describe

  #---------------------------------------------------------------------- mods
  describe file('/opt/chef/embedded/bin/bundle') do
    its(:content) { should match /load Gem\.bin_path/ }
  end # describe

  #------------------------------------------------------------------- helpers
  def docker_env(var)
    command("echo $#{var}").stdout
  end # docker_env

  def os_release
    command('rpm -q centos-release').stdout
  end # os_release

end # describe
